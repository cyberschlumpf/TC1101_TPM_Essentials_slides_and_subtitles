1
00:00:01,439 --> 00:00:06,080
Welcome to this OST2 lecture about the
secure storage of the TPM and its

2
00:00:06,080 --> 00:00:11,000
various use
cases. The nonvolatile memory of the TPM

3
00:00:11,000 --> 00:00:14,440
comes in various sizes and depends on
the TPM

4
00:00:14,440 --> 00:00:19,920
vendor. The NVRAM benefits from the TPM
authorization. And we can protect our

5
00:00:19,920 --> 00:00:25,400
data with password or TPM policy. The TPM
policy gives us the benefit of enhanced

6
00:00:25,400 --> 00:00:31,279
TPM authorization. And we cover this in
our advanced course. The TPM NVRAM

7
00:00:31,279 --> 00:00:38,520
provides four types of access. First and
foremost we have the operations in bulk

8
00:00:38,520 --> 00:00:43,760
where we can write it and read whole
chunks of bytes into the memory. This is

9
00:00:43,760 --> 00:00:51,280
limited depending on the vendors. Usually
the TPM can take up to 2048 bytes at once.

10
00:00:51,280 --> 00:00:57,120
But this really depends. We need to know
that the NVRAM of a TPM is measured in

11
00:00:57,120 --> 00:01:02,280
kilobytes. The intention here is to
provide secure storage for the most

12
00:01:02,280 --> 00:01:09,400
vital, critical, and sensitive data.
Another access way is to use the NVRAM

13
00:01:09,400 --> 00:01:16,360
as counter. This is a very powerful tool
because the only way to change the data

14
00:01:16,360 --> 00:01:22,159
is to use a dedicated TPM command that
can only increment the counter with one

15
00:01:22,159 --> 00:01:28,479
at a time. Also powerful is to set
separate bits using a bitwise OR

16
00:01:28,479 --> 00:01:32,720
operation. This access type will not be
covered as it is similar to the

17
00:01:32,720 --> 00:01:37,399
increment counter and using the
information from the one you can work it

18
00:01:37,399 --> 00:01:44,399
out on your own how to have an NV
index of type bits and how to use the NV

19
00:01:44,399 --> 00:01:49,200
set bits command. In this lecture we'll
cover the writing and reading in bulk

20
00:01:49,200 --> 00:01:55,960
and the use of NV counters. The last
type is a special type, where we can use

21
00:01:55,960 --> 00:02:01,640
the NVRAM as platform configuration
registers. PCRs are covered in our

22
00:02:01,640 --> 00:02:07,560
advanced course. Here is a typical
workflow for using the TPMs NVRAM.

23
00:02:07,560 --> 00:02:11,920
First we need to define an NV index. Which is
public information about the size of our

24
00:02:11,920 --> 00:02:16,800
data, and what type of authorization are
we going to use. Once we have an NV index

25
00:02:16,800 --> 00:02:20,920
we can perform one of the wrote
operations this could be write in bulk, 

26
00:02:20,920 --> 00:02:26,560
this could be the increment counter, or
one of the other two types. Remember that

27
00:02:26,560 --> 00:02:31,760
some write operations are not possible
depending on what type of NV

28
00:02:31,760 --> 00:02:37,319
index we chose and we'll see that in a
bit. We can always read our NV index as

29
00:02:37,319 --> 00:02:41,440
long as we provide the correct
authorization. Of course it does not make

30
00:02:41,440 --> 00:02:46,200
much sense to perform a read operation
before having any write operation.

31
00:02:46,200 --> 00:02:50,280
This is why I see this as the typical
workflow. Otherwise we will just read

32
00:02:50,280 --> 00:02:55,200
zeros. Depending on your use case if you
have stored data that needs to be there

33
00:02:55,200 --> 00:03:00,040
for the whole life cycle of the device
it is unlikely that you would want to

34
00:03:00,040 --> 00:03:08,319
undefine an NV index. Undefining the NV index
destroys the access to this data region.

35
00:03:08,319 --> 00:03:13,920
We would need to recreate our NV index
and it is unlikely we would be able to

36
00:03:13,920 --> 00:03:22,360
read out the data back. The undefined NV
index operation usually has consequences

37
00:03:22,360 --> 00:03:28,080
that go beyond from just missing the
public information about our data.

38
00:03:28,080 --> 00:03:33,720
The is essentially the delete operation of
our NVRAM data. There are use cases when

39
00:03:33,720 --> 00:03:40,799
we need to update or store a larger
portion of user data, of application data,

40
00:03:40,799 --> 00:03:47,319
configuration data. In these cases we
need to undefine our index and increase

41
00:03:47,319 --> 00:03:51,720
its size. Sometimes we would need to
change its authorization for various

42
00:03:51,720 --> 00:03:56,799
reasons. This is when having the
undefined command makes sense. In some

43
00:03:56,799 --> 00:04:03,200
scenarios it would not be used. Therefore
for this is an optional step depending

44
00:04:03,200 --> 00:04:09,599
on your use case. As mentioned there is
more than one type of NV index. As name

45
00:04:09,599 --> 00:04:15,040
suggest this (_ORDINARY) is the standard type. What
we can do write operations in bulk. Then

46
00:04:15,040 --> 00:04:21,960
we have the _COUNTER type which allows
only incremental changes to the NVRAM

47
00:04:21,960 --> 00:04:28,240
specified. One constraint here is that we
can have a maximum of eight byte

48
00:04:28,240 --> 00:04:35,800
values. This gives a 64 bits which is a
lot. This counter can be used for almost

49
00:04:35,800 --> 00:04:42,880
all use cases. It is rare to see a 64-bit
counter overflow, but again be aware that

50
00:04:42,880 --> 00:04:50,560
there's a limitation of the size of NV
index of type counter. The next two types

51
00:04:50,560 --> 00:04:57,720
_BITS and _EXTEND are for access that we
will not cover this. The _BITS type is

52
00:04:57,720 --> 00:05:04,080
again an 8 byte value
that can be changed only with bitwise OR

53
00:05:04,080 --> 00:05:08,199
operations, and there is a separate TPM
to command for this. We're going to go in

54
00:05:08,199 --> 00:05:13,960
detail about how to use an NV counter,
and going from there you can understand

55
00:05:13,960 --> 00:05:20,199
how to use _BITS just looking at the menu
of the tpm2-tool. The _EXTEND type is when

56
00:05:20,199 --> 00:05:27,199
we want to use the NVRAM as a PCR. And
because PCRs are limited on TPM this is

57
00:05:27,199 --> 00:05:34,120
a good way to extend how many PCRs we
have on our TPM. The last two types (_PIN_FAIL, _PIN_PASS) are

58
00:05:34,120 --> 00:05:40,600
very recent very modern with adding GPIOs
that can be controlled or read by the

59
00:05:40,600 --> 00:05:46,919
TPM. These new types will also not be
covered in this essentials class. The

60
00:05:46,919 --> 00:05:54,199
tpm2_ nvdefine tool helps us create NV
indexes. By default it sets the NV index

61
00:05:54,199 --> 00:06:00,400
type to _ORDINARY, which means we can use
write operations in bulk. Be aware that the

62
00:06:00,400 --> 00:06:05,360
maximum bulk size is typically about 2048
bytes. It is important that when we

63
00:06:05,360 --> 00:06:13,240
create our NV index we specify some sort
of password, so only we can read the data

64
00:06:13,240 --> 00:06:19,919
that is going to be stored. NV indexes
are created on demand. Therefore its

65
00:06:19,919 --> 00:06:27,039
numbering depends on how many NV
indexes we have so far. Unless we have

66
00:06:27,039 --> 00:06:32,639
designed a scheme where we know our
index zero will be for storing a

67
00:06:32,639 --> 00:06:37,120
certificate, our index one will be
storing for the system configuration, and

68
00:06:37,120 --> 00:06:44,280
so on and so on. It is usually not a bad
idea to go with a location by the tool

69
00:06:44,280 --> 00:06:49,440
where we just received the next
index. At the same time this could lead

70
00:06:49,440 --> 00:06:54,000
quickly to eat up the space that you
have, because you're not really keeping

71
00:06:54,000 --> 00:07:00,000
track of how much memory did you use.
Therefore I would recommend to design

72
00:07:00,000 --> 00:07:06,039
a really brief memory map of how much
NVRAM you have and what you want to store

73
00:07:06,039 --> 00:07:11,639
there, and define your own indexes.
This way you would know exactly what

74
00:07:11,639 --> 00:07:17,840
data is stored where and you can ask the
tool to generate a specific NV index

75
00:07:17,840 --> 00:07:26,160
with specific NV number. Indexes require
hierarchy to be tied to. Usually this is

76
00:07:26,160 --> 00:07:31,199
the owner's hierarchy. Once we have
created our NV index we can write data

77
00:07:31,199 --> 00:07:39,199
to it. The NV index is just a pointer. It
allocates for us space in the NVRAM.

78
00:07:39,199 --> 00:07:45,919
So in all tpm2-tools we would specify
which is our NV index. And the TPM will

79
00:07:45,919 --> 00:07:51,479
know where to place our data. Including
in cases where we don't write the

80
00:07:51,479 --> 00:07:57,919
complete size available to a certain NV
index. As we have in the example here.

81
00:07:57,919 --> 00:08:02,720
Let's assume that our document
that we're storing has only eight bytes.

82
00:08:02,720 --> 00:08:06,960
We would then want to write the
remaining 12 bytes. But we would again

83
00:08:06,960 --> 00:08:13,879
pass only the index, but then we would
pass the same index and adjust our

84
00:08:13,879 --> 00:08:19,720
offset setting, telling the tpm2-tool to
instruct the TPM to start writing from

85
00:08:19,720 --> 00:08:25,319
an offset of eight. It is mandatory to
specify an input data and this could

86
00:08:25,319 --> 00:08:30,039
come from a file or from the standard
input. Please note that if you want to

87
00:08:30,039 --> 00:08:34,440
use the standard input we need to
provide a dash immediately after the

88
00:08:34,440 --> 00:08:39,959
-i option, so everything is put
together. Once we have written our data

89
00:08:39,959 --> 00:08:45,839
to the NVRAM we would want to read it
out, to confirm the successful operation

90
00:08:45,839 --> 00:08:51,760
to verify. This can be performed with
another tpm2-tool. And what is important

91
00:08:51,760 --> 00:08:57,120
here is to provide the right
authorization. This means the right

92
00:08:57,120 --> 00:09:03,760
hierarchy, the right hierarchy
authorization, and the size. How many

93
00:09:03,760 --> 00:09:09,720
bytes do we want to read. Because if we
do not specify the number of bytes, we

94
00:09:09,720 --> 00:09:17,440
would receive the complete size of this
NV index. Let's say we have an NV index

95
00:09:17,440 --> 00:09:24,519
of 20 bytes and but we have stored only
eight, we will receive our original 8

96
00:09:24,519 --> 00:09:31,440
bytes, and then 12 bytes of undefined
data. Which could be zero, could be anything.

97
00:09:31,959 --> 00:09:38,880
Similar to key generation the
TPM has many internal types and flags.

98
00:09:38,880 --> 00:09:45,480
tpm2-tools do a good job of taking
care for most of these important flags

99
00:09:45,480 --> 00:09:52,240
and settings for us. In other cases, we
need to manually set some flags. One such

100
00:09:52,240 --> 00:09:58,440
case is the use of a counter, NV counter.
Here I have shown on the left a

101
00:09:58,440 --> 00:10:04,120
table in in the TCG
specification showing the most used

102
00:10:04,120 --> 00:10:10,920
flags which are _OWNERWRITE, _AUTHWRITE,
and _POLICYWRITE. Depending what is the

103
00:10:10,920 --> 00:10:17,240
type of authorization we
use, these flags help us specify are we

104
00:10:17,240 --> 00:10:21,160
going to have a password protection or
are we going to have a policy enhanced

105
00:10:21,160 --> 00:10:26,839
authorization for our NV
index. On the other hand the _OWNERWRITE

106
00:10:26,839 --> 00:10:33,959
is an important flag telling that the
NV index can be owned and used by the

107
00:10:33,959 --> 00:10:39,279
owner hierarchy. And at the bottom of
this table which, is not the end of the

108
00:10:39,279 --> 00:10:45,120
actual table, this is just where I cut
the screenshot, we have a field TPM_NT

109
00:10:45,120 --> 00:10:52,040
which defines the type of the
index. Remember tpm2_nvdefine creates

110
00:10:52,040 --> 00:10:58,240
an NV index of type _ORDINARY. If we want
any other type we would need to manually

111
00:10:58,240 --> 00:11:04,560
set this field. Before going further
and talking about the NV counter type, I

112
00:11:04,560 --> 00:11:10,160
want to address some of the other
interesting non-volatile bits and flags

113
00:11:10,160 --> 00:11:15,880
we have that you might want to set
manually at some point. I will highlight

114
00:11:15,880 --> 00:11:22,040
two of these options on the right. The
others I have put for completeness.

115
00:11:22,040 --> 00:11:26,880
The _NV_WRITEDEFINE and _NV_WRITELOCKED
are actually connected. The one (_NV_WRITEDEFINE) is the

116
00:11:26,880 --> 00:11:33,639
option where we instruct the TPM how the
NV index works. Saying that this NV

117
00:11:33,639 --> 00:11:40,200
index can be locked. And the _WRITELOCKED
is the actual flag telling to the TPM

118
00:11:40,200 --> 00:11:46,320
this NV index can no longer be written
to. Other interesting one is the _NV_WRITE_STCLEAR flag.

119
00:11:46,320 --> 00:11:53,320
This is very useful when
you want to write some data and as long

120
00:11:53,320 --> 00:12:00,079
as the device remains on, and working, the
data cannot be changed until the next

121
00:12:00,079 --> 00:12:06,560
time the system is rebooted. Perhaps we
should also mention the write-at-once bit (_NV_WRITEALL).

122
00:12:06,560 --> 00:12:12,800
By default this is not set, so you can
write the complete size of your NV index

123
00:12:12,800 --> 00:12:19,639
in multiple operations. But if this bit
is set then you are required to write

124
00:12:19,639 --> 00:12:24,519
all the data at once, meaning that if you
write less data then you cannot write

125
00:12:24,519 --> 00:12:29,480
any more data. Now let's talk about the
NV counter type. This is really a

126
00:12:29,480 --> 00:12:35,760
powerful tool where we can securely
track critical events, because of the TPM

127
00:12:35,760 --> 00:12:41,000
authorization. And we can increment this
counter only by one, only when providing

128
00:12:41,000 --> 00:12:45,959
the proper TPM2
authorization. To define such index we

129
00:12:45,959 --> 00:12:50,959
would need to do some extra work. We
would need to set the attributes for our

130
00:12:50,959 --> 00:12:57,560
NV index manually. And because we're
doing this, and we need to set that bit

131
00:12:57,560 --> 00:13:04,560
to have the NV counter type, we would also
need to set some other attributes.

132
00:13:04,560 --> 00:13:10,760
Otherwise we might not be able to access
the NV index. These two attributes are

133
00:13:10,760 --> 00:13:18,079
TPMA_NV_AUTHREAD and TPMA_NV_AUTHWRITE.
As the name suggests this essentially tells

134
00:13:18,079 --> 00:13:23,360
the TPM that if an auth value typically a
password is provided then we can read or

135
00:13:23,360 --> 00:13:31,600
write this NV index. And the (TPM)_NT field, as
shown earlier, specifies the type of this

136
00:13:31,600 --> 00:13:37,560
NV index. Having a value of one tells us
that this is an NV counter. You can go

137
00:13:37,560 --> 00:13:43,160
back and see the table again and you
would see the different values. Zero was

138
00:13:43,160 --> 00:13:48,160
_ORDINARY type of NV index, where we use
write operations in bulk.

139
00:13:48,160 --> 00:13:53,680
With the _NT being equal to one
we have an NV counter, which can only be

140
00:13:53,680 --> 00:14:01,120
incremented using a special command,
tpm2_nvincrement. Remember that the size of

141
00:14:01,120 --> 00:14:07,360
an NV counter is limited to 8 bytes.
This is also important when doing the (tpm2)_nvdefine

142
00:14:07,360 --> 00:14:14,600
operation. Notice the -s and 8
afterwards, specifying 8 bytes. 

143
00:14:14,600 --> 00:14:19,839
If you have any questions please write to us in
the OST2 discussion boards after each

144
00:14:19,839 --> 00:14:25,759
unit this helps us understand more of
the context of your question and where

145
00:14:25,759 --> 00:14:30,079
the difficulty
originated also feel free to drop us a

146
00:14:30,079 --> 00:14:34,880
message at the course's email

