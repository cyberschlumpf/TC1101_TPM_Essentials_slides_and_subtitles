1
00:00:00,799 --> 00:00:05,960
Welcome to this OST2 course about TPM
key generation using the wolfTPM

2
00:00:05,960 --> 00:00:08,359
software stack.

3
00:00:08,360 --> 00:00:13,639
So far we have used the tpm2-tools to interact with the TPM.

4
00:00:13,639 --> 00:00:18,199
If we want to create our own TPM application
we would need a TPM software stack or

5
00:00:18,199 --> 00:00:25,439
for short a TSS. The TSS provides for us
easy to use developer API. The developer

6
00:00:25,439 --> 00:00:31,119
API is much easier than the TPM command
transmission interface or for short

7
00:00:31,119 --> 00:00:37,280
TCTI. Usually we have a TPM driver that
takes care of the TCTI for us. And last

8
00:00:37,280 --> 00:00:41,160
but not least we have the physical layer.
If you're running an operating system

9
00:00:41,160 --> 00:00:45,360
there is already a driver that takes
care of the physical layer. Here is the

10
00:00:45,360 --> 00:00:51,640
typical sequence. Your application talks
to the TSS. The TSS in turn talks to the

11
00:00:51,640 --> 00:00:57,640
TPM driver. And the TPM driver, using probably
the OS drivers, communicates through the

12
00:00:57,640 --> 00:01:02,719
physical layer all the way to the TPM. The
response of the TPM walks through in the

13
00:01:02,719 --> 00:01:07,600
opposite direction back to your
application. Here is an example how the

14
00:01:07,600 --> 00:01:12,360
tpm2_create tool works under Linux to
talk to a physical TPM

15
00:01:12,360 --> 00:01:20,520
The tpm2_create tool uses the ESAPI of the tpm2-tss stack. The stack in

16
00:01:20,520 --> 00:01:26,119
turn talks to the Linux TPM driver.
The command is then sent over the physical

17
00:01:26,119 --> 00:01:32,079
layer using the Linux drivers for SPI or
I2C and reaches the TPM. When the

18
00:01:32,079 --> 00:01:36,880
TPM is ready it sends back a response
that is handled first, by the Linux

19
00:01:36,880 --> 00:01:43,840
driver, and then pass to the stack.
The stack once done unmarshalling the result

20
00:01:43,840 --> 00:01:49,079
returns the data to us.
Here is another example how we talk

21
00:01:49,079 --> 00:01:53,079
to a physical TPM in a different
situation, when we have an embedded

22
00:01:53,079 --> 00:01:58,840
system and bare metal firmware. In this
example we would be using the wolfTPM

23
00:01:58,840 --> 00:02:05,439
stack. The wolfTPM stack has wrapper API,
which makes it much easier than sending

24
00:02:05,439 --> 00:02:11,440
multiple TPM commments in a row.
By calling the wolfTPM_CreateKey

25
00:02:11,440 --> 00:02:17,879
wrapper API we're going to generate a
new child key. This command is processed

26
00:02:17,879 --> 00:02:24,120
through the API of the wolfTPM stack and it
reaches a custom user I/O callback function.

27
00:02:24,120 --> 00:02:28,800
This  I/O callback function can call an
already existing other firmware call that

28
00:02:28,800 --> 00:02:33,440
we have, or or it can implement the
complete solution to allow us to talk

29
00:02:33,440 --> 00:02:36,548
through the physical layer to the TPM chip.

30
00:02:36,548 --> 00:02:41,599
The communication, the response of the TPM,
walks in the opposite

31
00:02:41,599 --> 00:02:47,000
direction. For our lab environment we use
Docker and a TPM simulator. Because we

32
00:02:47,000 --> 00:02:51,360
use a simulator there is no need of a
TPM driver and there is no physical layer.

33
00:02:51,360 --> 00:02:57,360
The TPM simulator talks to us using TCP
socket. For wolfTPM we just specify it

34
00:02:57,360 --> 00:03:03,200
as a build option. Once built when we
issue the TPM2_Create command, and we

35
00:03:03,200 --> 00:03:08,120
address wolfTPM, it means it has to send it
over a TCP socket to talk to the TPM

36
00:03:08,120 --> 00:03:13,319
simulator. When the command is processed
it is sent back over TCP to wolfTPM and

37
00:03:13,319 --> 00:03:18,400
the result is provided to us.
Similar thing happens with the tpm2-tss. 

38
00:03:18,400 --> 00:03:22,720
The difference is that we need to specify as
a environment variable that we're going

39
00:03:22,720 --> 00:03:28,319
to use the simulator and at which port.
To save us time we do this when we run

40
00:03:28,319 --> 00:03:33,519
our Docker.
We need to use a TPM software stack or

41
00:03:33,519 --> 00:03:41,000
for short TSS. The TSS API is the most
developer friendly way to create

42
00:03:41,000 --> 00:03:46,760
applications that work together with the
TPM. There are two main cases when you

43
00:03:46,760 --> 00:03:52,360
look at today's landscape of TSS
available. The most popular are all open

44
00:03:52,360 --> 00:03:58,360
source. And only one complies with the
TCG specification the tpm2-tss.

45
00:03:58,360 --> 00:04:06,280
Every other stack usually has one
API mapping directly to the commands, and

46
00:04:06,280 --> 00:04:12,200
a second API, rich API, usually some kind
of wrappers or a layer on top of the

47
00:04:12,200 --> 00:04:17,160
one to one mapping where we can execute
direct actions with the TPM. Rather than

48
00:04:17,160 --> 00:04:25,320
sending multiple commands, we just send
one API call to our TSS of choice.

49
00:04:25,320 --> 00:04:32,160
Let's take a closer look the go-tpm originally
did not have one to one mapping API to

50
00:04:32,160 --> 00:04:38,880
the TPM commands. It was only a mild
layer on top that served as a rich API.

51
00:04:38,880 --> 00:04:44,800
Recently this was added. So now all the
other stacks that do not comply with the

52
00:04:44,800 --> 00:04:51,360
TCG specification for a similar pattern.
We have one to one mapping and the API

53
00:04:51,360 --> 00:04:56,639
itself sounds very familiar as if we're
reading the TCG specification. Let's say is

54
00:04:56,639 --> 00:05:05,479
it a TPM2_CreateKey, is it a TPM2_GetRandom,
or TPM2_StartAuthSession.

55
00:05:05,479 --> 00:05:11,600
What has proven over time is that the rich
API is preferred for professionals and

56
00:05:11,600 --> 00:05:19,120
enthusiasts. People just like using an
API that allows direct execution of a

57
00:05:19,120 --> 00:05:25,000
whole procedure, of whole operation,
rather than nitpicking all the commands.

58
00:05:25,000 --> 00:05:31,319
I personally prefer to this day the
SAPI, the system API, of the tpm2-tss,

59
00:05:31,319 --> 00:05:36,759
when it comes to using that stack. But it
is undoubtly that the ESAPI and FAPI

60
00:05:36,759 --> 00:05:41,000
are the preferred choice for developers
today. And this is true also for the

61
00:05:41,000 --> 00:05:47,960
other stacks. The IBM stack the go-tpm
stack, the wolfTPM stack, they all share

62
00:05:47,960 --> 00:05:54,080
the same design pattern. There is the
one to one mapping to TPM commands API

63
00:05:54,080 --> 00:05:58,840
that is there in case we need more
control, more granularity, more access to

64
00:05:58,840 --> 00:06:04,840
options flags and parameters. But each of
these stacks has its own rich API to

65
00:06:04,840 --> 00:06:10,520
perform whole operations, sometimes even
in sequence. The wolfTPM calls this Rich

66
00:06:10,520 --> 00:06:17,240
API "wrappers". Instead of having to do
five or six TPM commands we just issue

67
00:06:17,240 --> 00:06:23,360
one wolfTPM API that takes care of the
rest. This is a very similar approach to

68
00:06:23,360 --> 00:06:28,840
the ESAPI and especially the FAPI. FAPI
comes from feature API. Now the

69
00:06:28,840 --> 00:06:34,479
distinction between ESAPI and FAPI is
that FAPI is a very focused set of

70
00:06:34,479 --> 00:06:39,759
functions we can do. And on the other
hand ESAPI still gives us some control

71
00:06:39,759 --> 00:06:45,759
and allows us to do almost everything.
Where FAPI does not match the complete

72
00:06:45,759 --> 00:06:51,240
functionality that we can do with a TPM.
It performs the most common operations

73
00:06:51,240 --> 00:06:58,080
the most needed. In summary when choosing
a TPM stack the API would not be your

74
00:06:58,080 --> 00:07:03,160
first choice.
The most important aspect would be where

75
00:07:03,160 --> 00:07:07,479
I'm going to use this library in what
environment. Would that be Microsoft

76
00:07:07,479 --> 00:07:12,120
Windows, would that be Linux, would that
be a bare metal system or some kind of

77
00:07:12,120 --> 00:07:16,840
other embedded system. Is that embedded
system going to be a realtime operating

78
00:07:16,840 --> 00:07:22,080
system or Linux based device? Based on
that I would recommend to choose the

79
00:07:22,080 --> 00:07:29,000
stack. And then you can be more precise
in for example if you like more the

80
00:07:29,000 --> 00:07:36,451
wrapper or you prefer more the way FAPI
operates. Why I chose wolfTPM for this course?

81
00:07:36,451 --> 00:07:41,240
My background is industrial
automation. The systems I know are

82
00:07:41,240 --> 00:07:46,800
running real-time operating systems, they
have hard time meanings, sometimes they

83
00:07:46,800 --> 00:07:52,599
have really bare metal firmware, and even
nowadays from time to time I still have

84
00:07:52,599 --> 00:07:58,479
to use assembly. So from that point of
view I like the wolfTPM stack because

85
00:07:58,479 --> 00:08:03,919
it does not not require dynamic memory.
There's no heap needed. And this is major

86
00:08:03,919 --> 00:08:09,840
major importance for embedded systems
and safety from my perspective.

87
00:08:09,840 --> 00:08:14,960
That being said, if you're running a Linux
based embedded system, you're fairly well

88
00:08:14,960 --> 00:08:21,742
equipped to run the tpm2-tss with a ESAPI
and FAPI or and to even use the go-tpm stack. 

89
00:08:21,742 --> 00:08:27,605
What I also like is providing the
I/O communication with the TPM.

90
00:08:27,605 --> 00:08:32,800
Now when we have this embedded systems in these
industrial devices, usually we need to

91
00:08:32,800 --> 00:08:37,760
create our own custom I/O channels
because of the hardware.

92
00:08:37,760 --> 00:08:42,760
The communication interfaces operate
differently on the various hardware

93
00:08:42,760 --> 00:08:47,560
platforms. So often we write the
communication drivers ourselves.

94
00:08:47,560 --> 00:08:52,480
And linking that to the stack can be
difficult. In wolfTPM's case I like the

95
00:08:52,480 --> 00:08:57,680
fact this is done with a single callback
function. And we'll take a closer look in

96
00:08:57,680 --> 00:09:03,399
a moment. I like the wrapper API, because
it is simple and yet gives me all the

97
00:09:03,399 --> 00:09:07,720
rich functionality I need for digital
signing, for key creation with various

98
00:09:07,720 --> 00:09:14,399
options. So I have simplicity at the same
time I have optionality. And it is not

99
00:09:14,399 --> 00:09:20,160
the same as using the direct API and
using TPM commands one by one. But I

100
00:09:20,160 --> 00:09:24,680
like the balance I like the trade off there.
A fairly good amount of examples

101
00:09:24,680 --> 00:09:29,640
is available in the source code of the
stack itself. Naturally it's located in

102
00:09:29,640 --> 00:09:33,920
the examples folder. And you can find
very interesting cases like a

103
00:09:33,920 --> 00:09:40,079
TLS connection backed with TPM. You can find
different demos of sealing,

104
00:09:40,079 --> 00:09:45,519
key generation and so on and so on. Now the
examples have grown over time to the

105
00:09:45,519 --> 00:09:49,880
point where they're quite complex. They
support so many options and so many

106
00:09:49,880 --> 00:09:57,920
variants that what we will do here does
not translate and match. We would do a

107
00:09:57,920 --> 00:10:04,360
very fixed focused application using the
TPM. You want to generate an RSA key

108
00:10:04,360 --> 00:10:08,320
that's what we're going to do. There will
be no options no arguments and so on so on.

109
00:10:08,320 --> 00:10:12,399
Looking at the examples code at times
could be a bit overwhelming until you

110
00:10:12,399 --> 00:10:17,839
get what you want and until you find out
hey how this works. But they're there and

111
00:10:17,839 --> 00:10:21,760
they support a lot of the TPM
functionality, so they are a really good

112
00:10:21,760 --> 00:10:26,560
source of information. I just think that
for starters we would like to have this

113
00:10:26,560 --> 00:10:34,600
monolithic structure for our examples.
And this is how we built the exercises

114
00:10:34,600 --> 00:10:40,360
for this part of the course. Last but not
least there is a good API menu that is

115
00:10:40,360 --> 00:10:45,279
built using Doxygen and you can find
it online at this link showed on the

116
00:10:45,279 --> 00:10:47,839
slide.

