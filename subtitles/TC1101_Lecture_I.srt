1
00:00:00,520 --> 00:00:06,319
Welcome to this OST2 lecture about TPM
capabilities and TPM persistence. These

2
00:00:06,319 --> 00:00:12,519
are two important topics because TPM
come in different forms. Based on the TPM

3
00:00:12,519 --> 00:00:18,760
vendor that manufactured your TPM, it
would have more memory or a bit

4
00:00:18,760 --> 00:00:23,160
different set of commands. To learn
about this these qualities there is an

5
00:00:23,160 --> 00:00:29,480
internal tpm2 command that we can use.
TPM persistence on the hand is some kind

6
00:00:29,480 --> 00:00:35,680
of an optimization that enables us to
maintain important TPM objects in the

7
00:00:35,680 --> 00:00:41,559
TPM memory loaded and ready for use
between power cycles. As we experienced

8
00:00:41,559 --> 00:00:47,800
in this course most TPM objects are
loaded in a temporary memory. Most TPM

9
00:00:47,800 --> 00:00:53,320
objects are transient. Once the system is
shut down, power cycle off, we no longer

10
00:00:53,320 --> 00:00:58,440
have access to the keys after the system
is backed up is booted again. This means

11
00:00:58,440 --> 00:01:02,600
we need to load our keys again again. We
need to authenticate to load them.

12
00:01:02,600 --> 00:01:07,720
In some cases this is very time sensitive.
Especially if it's a primary key.

13
00:01:07,720 --> 00:01:15,600
For example a primary TPM key with RSA 2048
bit can take a lot of time, because TPMs

14
00:01:15,600 --> 00:01:20,920
are built for security not speed.
In these situations we can have between

15
00:01:20,920 --> 00:01:26,159
three to seven key slots on the TPM for
persistence. This depends on the TPM

16
00:01:26,159 --> 00:01:31,119
vendor as we mentioned earlier. And this
operation might look very attractive as

17
00:01:31,119 --> 00:01:36,680
a good tradeoff. In some systems like an
automotive system you can have up to thousand

18
00:01:36,680 --> 00:01:43,520
keys. Seven keys slots is not that much.
This is why we need to be careful and

19
00:01:43,520 --> 00:01:48,439
considerate when using persistence
command. Also, when we persist a TPM

20
00:01:48,439 --> 00:01:54,159
object it will be there regardless how
many times we turn on and off the system,

21
00:01:54,159 --> 00:01:59,680
how many times we power on and off the
TPM. This object will remain loaded.

22
00:01:59,680 --> 00:02:04,159
That is is the benefit of this operation. But
at the same time it means that if we

23
00:02:04,159 --> 00:02:08,800
need to load a different key we need
first to remove this key. This operation

24
00:02:08,800 --> 00:02:14,959
is called evicting. And the name of
this command tpm2_evictcontrol.

25
00:02:14,959 --> 00:02:20,239
Now this operation has its benefits that are
just very sound in different scenarios.

26
00:02:20,239 --> 00:02:25,000
For example you're securing a
communication protocol using the TPM,

27
00:02:25,000 --> 00:02:29,959
therefore you need your digital signing
key available at all times. You do not

28
00:02:29,959 --> 00:02:35,040
want to have latency for the digital
signing operation to load the key every

29
00:02:35,040 --> 00:02:41,599
time, between reboots or for other reason.
This is a valid case. You just have to

30
00:02:41,599 --> 00:02:47,480
keep in mind that this there is a very
limited storage for such operation as

31
00:02:47,480 --> 00:02:52,640
persistence. Let's look at the command.
Persisting a key is as easy as removing

32
00:02:52,640 --> 00:02:58,319
the key. This is why we need to make sure
that there is authorization control for

33
00:02:58,319 --> 00:03:03,840
the hierarchy and then (or else) our security
will be compromised. To change the

34
00:03:03,840 --> 00:03:09,000
authorization of a hierarchy or any TPM
object there is a special command called

35
00:03:09,000 --> 00:03:14,599
tpm2_changeauth. We will look at this
command deeper in the next slide. Once we

36
00:03:14,599 --> 00:03:19,000
set an authorization to the hierarchy,
then what's important is to have our

37
00:03:19,000 --> 00:03:23,519
object created or loaded to be in
transient memory. and then to instruct

38
00:03:23,519 --> 00:03:28,879
the TPM "This is our object, this is the
authorization for this object. please

39
00:03:28,879 --> 00:03:33,239
make it permanent".
And you see the example on the slide and

40
00:03:33,239 --> 00:03:38,200
the typical output when you have a
success. You receive back a handle.

41
00:03:38,200 --> 00:03:42,360
This is a number of the slot where the key
was put. You have the option to select

42
00:03:42,360 --> 00:03:47,439
which slots do you want, but typically
it's best to either allocate the next

43
00:03:47,439 --> 00:03:54,200
available slot, and handle the response,
or have a predefined set of the slots.

44
00:03:54,200 --> 00:03:59,519
Meaning that I'll have my digital
signing key at slot zero, I have my

45
00:03:59,519 --> 00:04:04,799
OS key slot one, I have my attestation
key at slot two, and then the rest of the

46
00:04:04,799 --> 00:04:08,200
slots if available I'll just know
they're available if I need to do

47
00:04:08,200 --> 00:04:13,200
something with more keys. This is a very
good approach to have it designed in

48
00:04:13,200 --> 00:04:18,799
advance. Of course, again, it is rarely the
case that we need to persist so many keys.

49
00:04:18,799 --> 00:04:23,680
Depending on your scenario you have to
make that choice. Now let's take a closer

50
00:04:23,680 --> 00:04:28,280
look at the (tpm2)_changeauth command that we
used in this example. The (tpm2)_changeauth

51
00:04:28,280 --> 00:04:32,759
command is very important and very
simple at the same time. It takes just a

52
00:04:32,759 --> 00:04:39,160
few parameters. Usually we have a
hierarchy to select using the--c

53
00:04:39,160 --> 00:04:43,919
parameter, and then we need to set a
different authorization, usually with

54
00:04:43,919 --> 00:04:47,800
the -p which is some kind of a pass
phrase. On the slide you can see

55
00:04:47,800 --> 00:04:51,639
different examples of changing the
authorization or removing it all

56
00:04:51,639 --> 00:04:55,479
together. Maybe for development purposes
you need to have an empty password

57
00:04:55,479 --> 00:05:00,039
temporarily there's a way to do that. Of
course the tpm2_changeauth command can

58
00:05:00,039 --> 00:05:04,759
be used not only on hierarchies but also
on TPM object. The important thing is for

59
00:05:04,759 --> 00:05:09,320
them to be already loaded. And then we
need to make sure that we provide also

60
00:05:09,320 --> 00:05:15,320
the parent to that TPM object when we
want to change its authorization.

61
00:05:15,320 --> 00:05:19,840
This command supports parameter encryption,
because it works with sensitive data.

62
00:05:19,840 --> 00:05:26,000
To pass a policy session or HMAC session to
enable parameter encryption use the -c

63
00:05:26,000 --> 00:05:33,280
argument. And do not forget when creating
a session, with tpm2_startauthsession,

64
00:05:33,280 --> 00:05:39,919
to also include a primary object to add
bind and salt to your session. This makes

65
00:05:39,919 --> 00:05:44,000
the parameter encryption really stronger
and gives you the highest guarantee

66
00:05:44,000 --> 00:05:48,840
against Machine-in-the-Middle attacks.
Now let's get back to our persistent

67
00:05:48,840 --> 00:05:54,280
object that we made in the previous
slide. For some reason we need to remove

68
00:05:54,280 --> 00:06:00,560
our persistent key. It could be that we
are rotating keys, it could be that we

69
00:06:00,560 --> 00:06:07,400
run out of persistent key slots. And for
the next process on our system we need a

70
00:06:07,400 --> 00:06:12,960
different key. Maybe it's it is an
upgrade process. This could happen more

71
00:06:12,960 --> 00:06:17,759
often than you think. In this situation
it is important to know that we can use

72
00:06:17,759 --> 00:06:23,960
the same TPM evict command as last
time: tpm2_evictcontrol. We need to

73
00:06:23,960 --> 00:06:28,560
provide the hierarchy of the object that
we need to evict and the proper

74
00:06:28,560 --> 00:06:33,680
authorization. Remember, if you haven't
set an authorization for the hierarchy

75
00:06:33,680 --> 00:06:38,240
then you have an empty password, which is
okay to try the command, but this also

76
00:06:38,240 --> 00:06:44,039
means that anyone on the system can
remove your persistent key, and thus

77
00:06:44,039 --> 00:06:49,479
break the system. So it is important if
you decide to persist a key, make sure to

78
00:06:49,479 --> 00:06:55,360
set the hierarchy authorization. This is
a recommendation in all cases. But just

79
00:06:55,360 --> 00:06:59,840
even as you're developing it's good to
have that already built in your code or

80
00:06:59,840 --> 00:07:05,800
your flow. Then the last parameter is
very important as well, the handle.

81
00:07:05,800 --> 00:07:10,000
Remember this is the outcome of the
first time we ran the command. This was

82
00:07:10,000 --> 00:07:18,240
the output on upon success. We were given
the address where our key is persisted.

83
00:07:18,240 --> 00:07:22,919
Usually this is one of the key slots
so only the last digit of the handle

84
00:07:22,919 --> 00:07:29,000
number differs. But it is good to have
that number somewhere written, stored, or

85
00:07:29,000 --> 00:07:34,777
just in general as part of your application
defined. So you can then remove the key

86
00:07:34,777 --> 00:07:40,284
and remove the right key. Here we come to
the tpm2 capabilities

87
00:07:40,284 --> 00:07:46,240
If you notice on the previous slide there is a command
that helps you find out what are the

88
00:07:46,240 --> 00:07:54,000
persistent handles currently in use on
this TPM. And that's valuable, especially

89
00:07:54,000 --> 00:07:59,319
if in the course of using the TPM, maybe
there are other users other processes that

90
00:07:59,319 --> 00:08:03,199
that are using it. This is why as we
mentioned it's important to have

91
00:08:03,199 --> 00:08:07,639
different hierarchies, different
hierarchies authorizations, and so on and

92
00:08:07,639 --> 00:08:14,560
so on. Now the command comes with many
options. In my experience most frequently

93
00:08:14,560 --> 00:08:20,840
I need to check either for a command or
for a supported algorithm. And we'll look

94
00:08:20,840 --> 00:08:26,280
closer at the various things we can
list. I definitely recommend looking at

95
00:08:26,280 --> 00:08:30,639
the help of this command because there
you can see all the supported

96
00:08:30,639 --> 00:08:36,159
capabilities that we can list. Handles
also something that very often can come

97
00:08:36,159 --> 00:08:41,479
to mind what are the current handles in
use transient, persistent, NV indexes,

98
00:08:41,479 --> 00:08:47,560
and so on. Very useful command especially
during development but not only. During

99
00:08:47,560 --> 00:08:53,560
production you can also use this command
to make some decisions. Depending on how

100
00:08:53,560 --> 00:08:59,760
many users are on the platform. This is a
big factor usually. And another

101
00:08:59,760 --> 00:09:07,040
I think interesting aspect of the
command is the properties. There're two

102
00:09:07,040 --> 00:09:11,920
sets of this command there's the
tpm2_getcap properties-fixed and

103
00:09:11,920 --> 00:09:17,000
properties-variable. Properties-fixed are properties that we cannot change.

104
00:09:17,000 --> 00:09:22,440
This is how the TPMs was manufactured. Where
the properties-variable shows us some of

105
00:09:22,440 --> 00:09:28,760
the capabilities of the TPM that are
already in use or have not been used.

106
00:09:28,760 --> 00:09:34,360
For example here we can see if we have set
an authorization to the owner hierarchy,

107
00:09:34,360 --> 00:09:38,959
or the endorsement hierarchy. In this
screenshot you can clearly see that the

108
00:09:38,959 --> 00:09:44,600
owner hierarchy has its authorization
changed from the default empty. And for

109
00:09:44,600 --> 00:09:49,399
the endorsement authorization we have
not done so. Also good to see if it's a

110
00:09:49,399 --> 00:09:55,079
managed machine you can visualize
which of the hierarchies are currently

111
00:09:55,079 --> 00:10:00,160
available on the system. This is
important when you have a managed system,

112
00:10:00,160 --> 00:10:05,880
or a more closed system that uses a TPM.
But still you have access to the TPM to

113
00:10:05,880 --> 00:10:11,040
secure your third party application.
I think it is great to start from just

114
00:10:11,040 --> 00:10:16,760
listing the available subcommands to
the tpm2_getcap tool. I definitely

115
00:10:16,760 --> 00:10:21,407
suggest trying all of the available
options and just looking through.

116
00:10:21,407 --> 00:10:25,839
The tpm2_getcap algorithms is very
interesting. You would see different

117
00:10:25,839 --> 00:10:29,800
codes for the different algorithms.
But it gives you a sense of what available.

118
00:10:29,800 --> 00:10:35,436
Especially if you are using symmetric
algorithms. They are export regulated.

119
00:10:35,436 --> 00:10:40,000
So your TPM most likely will have a
different set from a different

120
00:10:40,000 --> 00:10:45,600
manufacturer ordered from a different
place and so on and so on. So I highly

121
00:10:45,600 --> 00:10:50,120
recommend using the (tpm2_)getcap command on
the TPM before starting development, just

122
00:10:50,120 --> 00:10:57,320
to see what you have on the TPM that
you've chosen for your design and your

123
00:10:57,320 --> 00:11:00,320
system.

