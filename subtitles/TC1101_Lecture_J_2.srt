1
00:00:00,040 --> 00:00:05,120
To start working with the software stack
we usually need to initialize it. In some

2
00:00:05,120 --> 00:00:09,320
cases this happens in two steps: first we
initialize the stack, then we open the

3
00:00:09,320 --> 00:00:14,639
TPM device or the communication channel
to the TPM. Here in wolfTPM we have a

4
00:00:14,639 --> 00:00:21,199
single API call to do both at the same
time. Also the wolfTPM init usually

5
00:00:21,199 --> 00:00:26,039
takes care of the start of the TPM.
Remember that when we were using the

6
00:00:26,039 --> 00:00:30,519
simulator we had to manually issue the
tpm2_startup command.

7
00:00:30,519 --> 00:00:37,239
The important detail here is the
I/O callback. The (wolfTPM2_)Init function takes

8
00:00:37,239 --> 00:00:41,960
pointer to a function that implements
this callback. And there are several

9
00:00:41,960 --> 00:00:46,760
examples how to do this. Linux is
supported out of the box, QNX as well,

10
00:00:46,760 --> 00:00:51,280
a barebox, and so on and so on.
This means that we can start using wolfTPM in

11
00:00:51,280 --> 00:00:55,680
Linux environment straight away. This way
we'll be interacting with the Linux

12
00:00:55,680 --> 00:01:00,600
kernel driver for the TPM, and we can
just focus on issuing the commands.

13
00:01:00,600 --> 00:01:04,960
We talk about the template application that
we're providing for you to use during

14
00:01:04,960 --> 00:01:11,759
the exercises. I just want to assure you
that the TPM I/O callback is taken care of.

15
00:01:11,759 --> 00:01:19,880
So we can really focus on the exercise
goal. The typical workflow for generating

16
00:01:19,880 --> 00:01:26,759
a primary key is after initializing
the wolfTPM stack to set authorization for

17
00:01:26,759 --> 00:01:31,640
this key. Remember without authorization
anyone could load that key. Now here we

18
00:01:31,640 --> 00:01:38,040
have several wrappers that we can use.
The main one is (wolfTPM2_)SetAuth but the one that

19
00:01:38,040 --> 00:01:43,280
I've chosen here is (wolfTPM2_)SetAuthPassword.
This already takes care of the

20
00:01:43,280 --> 00:01:47,479
operations needed to prepare the
authorization slot for password. With the

21
00:01:47,479 --> 00:01:53,616
proper tag ,with a proper flag, and just
place our password as string in the right place.

22
00:01:53,616 --> 00:01:57,880
So when we run the command for
generating the key when we actually said

23
00:01:57,880 --> 00:02:02,920
to the TPM "please generate our primary
key" it can process that information and

24
00:02:02,920 --> 00:02:07,560
our key gets that password as a
requirement. The next step naturally is

25
00:02:07,560 --> 00:02:12,280
to select our key type. And the key type
if you remember we discussed this is

26
00:02:12,280 --> 00:02:18,519
part of the key template. This is where
the seed goes, this is where we specify

27
00:02:18,519 --> 00:02:21,965
different properties of the key and
we'll take a closer look in a moment.

28
00:02:21,965 --> 00:02:27,400
And finally we are ready to use the wolfTPM
wrapper for creating a primary key (wolfTPM2_CreatePrimaryKey).

29
00:02:27,400 --> 00:02:31,829
Everything that we prepared in the
previous two steps, the authorization slot, 

30
00:02:31,829 --> 00:02:36,934
the key template, will go into this
function. Let's take a closer look.

31
00:02:36,934 --> 00:02:42,000
The are three authorization slots for each
command. Usually we may have one

32
00:02:42,000 --> 00:02:47,040
authorization slot for the primary key,
one authorization slot for the child key,

33
00:02:47,040 --> 00:02:52,726
and the last one we could use for HMAC
session or parameter encryption and so on.

34
00:02:52,726 --> 00:02:58,400
Often we would need only one of these
authorization slots. And of course I'm

35
00:02:58,400 --> 00:03:02,200
just assuming that we all always have an
HMAC session, so the second one will be by

36
00:03:02,200 --> 00:03:08,440
default taken as well. In here we need to
again provide the device context. And you

37
00:03:08,440 --> 00:03:14,400
notice that most wolfTPM wrappers
require this as the first parameter.

38
00:03:14,400 --> 00:03:19,200
This is the way the wolfTPM handles
information about the current TPM device.

39
00:03:19,200 --> 00:03:22,599
This means that you actually can be
communicating to multiple TPMs on your

40
00:03:22,599 --> 00:03:26,239
system. Especially if you're in a server
environment you can have multiple

41
00:03:26,239 --> 00:03:32,200
virtual TPMs. And each of these TPMs will
have it's own WOLFTPM2_DEV

42
00:03:32,200 --> 00:03:40,840
structure. Now if you are unsure of where
to place the authorization slot, at which

43
00:03:40,840 --> 00:03:46,840
number, it's usually to start from a
clean state. So start at zero. You have

44
00:03:46,840 --> 00:03:52,560
three slots, zero, one, and two. And this is a
bit the manual part of using wolfTPM.

45
00:03:52,560 --> 00:03:57,480
But I personally like it because this
gives me control over the operations.

46
00:03:57,480 --> 00:04:03,120
It is in a way similar to using maybe SAPI,
but at the same time I get all the rich

47
00:04:03,120 --> 00:04:08,799
applications of FAPI. If I have to
compare between the two stacks. So I

48
00:04:08,799 --> 00:04:13,079
think it's a nice trade-off I think it's
a nice balance. But again I come from the

49
00:04:13,079 --> 00:04:17,759
industrial space from the embedded
systems so this is just my preference.

50
00:04:17,759 --> 00:04:25,080
In contrast the ESAPI and FAPI API provide
almost automatic parameter encryption

51
00:04:25,080 --> 00:04:28,919
enabled throughout the whole
communication which is great. Here we'll

52
00:04:28,919 --> 00:04:36,880
have to manually see how to do that with
wolfTPM stack. And it takes just two API

53
00:04:36,880 --> 00:04:40,680
calls but still it's something that we
need to do. It's not that the stack is

54
00:04:40,680 --> 00:04:44,759
taken care of it by itself. And the
string operations here are just for

55
00:04:44,759 --> 00:04:49,919
example how we can set that auth value.
Now that we have prepared our

56
00:04:49,919 --> 00:04:54,520
authorization for the new key that we
want to generate, we need to prepare the

57
00:04:54,520 --> 00:05:01,520
template that the TPM will use to
actually generate our key. And there are

58
00:05:01,520 --> 00:05:07,759
many options that can go into this. For
our ease we use the wrapper that just gives

59
00:05:07,759 --> 00:05:15,000
us a template for 2048 bit RSA key. And
notice this SRK abbreviation. This means

60
00:05:15,000 --> 00:05:21,160
storage key. This is a leftover
terminology from the days of TPM 1.2

61
00:05:21,160 --> 00:05:26,560
that is now deprecated. Now we're using
TPM 2.0 but this terminology remained

62
00:05:26,560 --> 00:05:31,560
because it was very familiar to
developers. Nowadays we just use primary

63
00:05:31,560 --> 00:05:36,520
keys under the owner hierarchy for our
application keys for our user keys and

64
00:05:36,520 --> 00:05:42,639
so on. Now that we have our key template
created we can move forward with the key

65
00:05:42,639 --> 00:05:47,639
generation. Just two more parameters that
are important. We need some kind of key

66
00:05:47,639 --> 00:05:52,639
authorization and the simplest form is
to have a password authorization.

67
00:05:52,639 --> 00:05:57,280
For this we need to provide a string and the
size of that string which is then passed

68
00:05:57,280 --> 00:06:03,120
to the TPM as a requirement to be
embedded into the key. Also when asking

69
00:06:03,120 --> 00:06:08,840
the key to create a primary object we
need to specify under which hierarchy.

70
00:06:08,840 --> 00:06:14,280
And as just mentioned this usually the
owner hierarchy. Other than that we need

71
00:06:14,280 --> 00:06:20,440
to have a variable to hold the result of
the operation. Remember primary key

72
00:06:20,440 --> 00:06:25,880
material does not leave the TPM. The
private part remains inside the TPM ,and

73
00:06:25,880 --> 00:06:32,120
we're provided only with a handle, an
index, to where this primary key lives

74
00:06:32,120 --> 00:06:37,880
inside the TPM it was generated and
loaded. The public part of this key will

75
00:06:37,880 --> 00:06:43,000
be given to us as part of the response
of the command. We are ready to generate

76
00:06:43,000 --> 00:06:49,000
our child key now that we have a primary
object. This will enable the TPM to wrap

77
00:06:49,000 --> 00:06:54,493
our child key once generated, so it is in
encrypted form outside the TPM.

78
00:06:54,493 --> 00:06:59,039
In contrast with the primary object for the
child key will receive both the private

79
00:06:59,039 --> 00:07:03,639
material and the public material. But the
private material will come in encrypted

80
00:07:03,639 --> 00:07:09,560
form. So even if we decide to store it
outside our application and the TPM as a

81
00:07:09,560 --> 00:07:16,021
file on the disk it is going to be safe.
Only the TPM can load that key.

82
00:07:16,021 --> 00:07:21,800
The path is similar to creating a primary key. We
need a key template but this time with

83
00:07:21,800 --> 00:07:30,080
the proper tags for a child key. And then
we need authorization for our new key.

84
00:07:30,080 --> 00:07:33,840
It is recommended to have different
authorization for the primary key and

85
00:07:33,840 --> 00:07:39,879
the child key. Although in some scenarios
you would see that having authorization

86
00:07:39,879 --> 00:07:45,440
for the primary key might be enough,
given that you have only one child key

87
00:07:45,440 --> 00:07:49,520
underneath. So by having the
authorization to the primary key you're

88
00:07:49,520 --> 00:07:54,319
the only one who can load the child key.
Still, better to have separate

89
00:07:54,319 --> 00:08:00,759
authorizations for primary and child keys.
Here we have more than two templates.

90
00:08:00,759 --> 00:08:07,560
Primary keys are limited only RSA and ECC.
Child keys can be symmetric or keyed hash

91
00:08:07,560 --> 00:08:12,199
as well.
We have defaults that are good and

92
00:08:12,199 --> 00:08:17,360
we can just start using them. If we need
to set more attributes from the TPMA_OBJECT

93
00:08:17,360 --> 00:08:22,400
structure we can do that.
We talked about this in a previous lecture.

94
00:08:22,400 --> 00:08:27,280
And also we took a look at the TCG
library specification part two that

95
00:08:27,280 --> 00:08:31,919
describes the different structures that
go into the TPM communication.

96
00:08:31,919 --> 00:08:37,680
For example with the ECC template we need to
specify a curve. We can use the default

97
00:08:37,680 --> 00:08:44,959
one or pick one from the common values.
Signature scheme is also necessary.

98
00:08:44,959 --> 00:08:50,240
We also discussed this in a previous
lecture. In some cases we can even select

99
00:08:50,240 --> 00:08:56,640
a NULL signature scheme. But I would
recommend having at least ECDSA or other

100
00:08:56,640 --> 00:09:02,800
popular scheme. The symmetric template
requires us to provide a bit more

101
00:09:02,800 --> 00:09:08,079
information. Here we need to set what is
the mode of the symmetric operation and

102
00:09:08,079 --> 00:09:15,240
for AES it could be CFB, CBC, and so on. Also
depends on what your TPM actually

103
00:09:15,240 --> 00:09:20,760
supports. You can check this with the (tpm2_)getcap
command that we already learned.

104
00:09:20,760 --> 00:09:27,880
Usually the default is as CFB. But just
to be sure make sure you specify the

105
00:09:27,880 --> 00:09:35,894
mode you want. Key bits naturally we have
128 bit or 192 bit or more AES key.

106
00:09:35,894 --> 00:09:40,120
Other than that we need to define is this
going to be used for signing or

107
00:09:40,120 --> 00:09:45,860
decryption. This helps the template
function, the wrapper, to set the proper

108
00:09:45,860 --> 00:09:48,180
flags for our key. 

109
00:09:48,180 --> 00:09:50,289
Let's talk for a moment about handles.
Because when we

110
00:09:50,289 --> 00:09:57,959
were using the tpm2-tools we were using
mostly saved TPM context. Where in reality

111
00:09:57,959 --> 00:10:02,320
the TPM uses
numbering scheme to differentiate

112
00:10:02,320 --> 00:10:07,839
between the different objects. It also
helps us understand, "Is this a transient

113
00:10:07,839 --> 00:10:13,519
object that will be lost once the power
is turned off or is this a persistent

114
00:10:13,519 --> 00:10:17,640
object that will remain after power off?".
It also helps us understand when this

115
00:10:17,640 --> 00:10:22,480
object lives in the NVRAM or when this
is a permanent object that came with the

116
00:10:22,480 --> 00:10:27,760
TPM; it is manufactured this way. So the
interesting thing is that all handles

117
00:10:27,760 --> 00:10:32,720
have the same length 32 bit. And we can
identify the type of the handle by the

118
00:10:32,720 --> 00:10:39,519
first eight bits the most significant.
I have put here the four most important

119
00:10:39,519 --> 00:10:43,720
handle types. There are few more, for
example for platform configuration

120
00:10:43,720 --> 00:10:48,480
registers, but this will be part of our
advanced course. So when you see a handle

121
00:10:48,480 --> 00:10:52,760
starting with 80 in hex at the
beginning ,you can be sure that this is a

122
00:10:52,760 --> 00:10:57,720
transient object, this is probably a TPM
key we created and loaded. And if we

123
00:10:57,720 --> 00:11:01,720
don't use the
command, we will not make this a

124
00:11:01,720 --> 00:11:07,560
persistent object. So it will be lost at
power cycle. When you use the

125
00:11:07,560 --> 00:11:12,760
tpm2_evictcontrol command and in our exercises you
probably noticed that the handle which

126
00:11:12,760 --> 00:11:18,880
was in the output started with 81 in hex.
And then we have the NVRAM. Which is

127
00:11:18,880 --> 00:11:25,200
when we did all of these exercises about
using NV indices and NV counter.

128
00:11:25,200 --> 00:11:32,519
And there the index started with 01.
We need to know about handles now that

129
00:11:32,519 --> 00:11:36,680
we're going to use the API, because in
certain operations, like when creating a

130
00:11:36,680 --> 00:11:42,360
child key, we would need to provide to
the TPM the index, the location, of the

131
00:11:42,360 --> 00:11:47,720
key slot where our primary object is
loaded. So the TPM can use that object to

132
00:11:47,720 --> 00:11:53,040
generate the child key and also wrap
encrypt our child key so it comes in a

133
00:11:53,040 --> 00:11:59,120
protected form. It is unlikely that you
have to remember all these numbers but

134
00:11:59,120 --> 00:12:05,120
during debugging or testing you would
see an index coming out of the TPM or

135
00:12:05,120 --> 00:12:09,720
coming out of your program and it's just
easier to know the beginning of the

136
00:12:09,720 --> 00:12:14,800
indexes so you know "oh this is the
primary key" or "this is my evictcontrol

137
00:12:14,800 --> 00:12:21,360
command failing" or "this is an object in
NVRAM" or something else. So although

138
00:12:21,360 --> 00:12:25,839
these indexes are needed we will usually
assign them to a variable in the

139
00:12:25,839 --> 00:12:29,519
previous call and then use them in the
next API call immediately or something

140
00:12:29,519 --> 00:12:34,480
like that. So the indexes will not be
something we really operate on, like will

141
00:12:34,480 --> 00:12:38,639
not be increasing or decreasing the
indexes, it is what the TPM will provide

142
00:12:38,639 --> 00:12:42,839
for us. The TPM is the only entity that
can actually generate this unless we

143
00:12:42,839 --> 00:12:49,240
want to specify for example "I want this
NV index at offset or at this number".

144
00:12:49,240 --> 00:12:52,920
But it has to be in that range. The TPM
will be the one that will confirm this

145
00:12:52,920 --> 00:12:58,240
request. And here is the create key
wrapper. You immediately notice that the

146
00:12:58,240 --> 00:13:01,720
structure that holds the child keys
different from the structure that we

147
00:13:01,720 --> 00:13:06,800
used for the for the primary key. The
reason is that we receive the private

148
00:13:06,800 --> 00:13:11,480
material for the child key. Of course it
comes in a protected form. But this

149
00:13:11,480 --> 00:13:16,600
requires a different structure. Now that
we have our public template, and we also

150
00:13:16,600 --> 00:13:22,440
have prepared a separate authorization,
we can just use that wrapper to add the

151
00:13:22,440 --> 00:13:27,440
TPM to create the new key. And remember
in this case we no longer have to

152
00:13:27,440 --> 00:13:33,079
specify the hierarchy as a parent, but we
need to specify the primary key handle

153
00:13:33,079 --> 00:13:38,639
that we created. When trying to solve the
exercises in this part of the course,

154
00:13:38,639 --> 00:13:45,120
please use our template, because it gives
you a quick start into building a simple

155
00:13:45,120 --> 00:13:49,480
wolfTPM application.
This contains two main parts.

156
00:13:49,480 --> 00:13:55,680
One is the actual application talking to
the TPM. And the other is the TPM I/O callback

157
00:13:55,680 --> 00:14:01,759
that we took from the example
callback for Linux. Because our Docker

158
00:14:01,759 --> 00:14:07,160
environment runs in Linux this works
very well. The changes are expected to

159
00:14:07,160 --> 00:14:12,480
happen only in the main application. You
should not have to do anything with the

160
00:14:12,480 --> 00:14:19,199
TPM I/O callback. this means that the wolfTPM
init code should work as is. We also

161
00:14:19,199 --> 00:14:24,079
provide a makefile to make it easier to
immediately build the application.

162
00:14:24,079 --> 00:14:28,800
So once you have changes just make sure to
rebuild, use make clean however you decide.

163
00:14:28,800 --> 00:14:34,600
In case you face problems it's
good to check the TPM log that the TPM

164
00:14:34,600 --> 00:14:39,600
simulator creates. Sometimes there is
important information in there. And we

165
00:14:39,600 --> 00:14:45,680
can have more verbose output for the
simulator. What I have found in my

166
00:14:45,680 --> 00:14:50,079
practice is that having more verbose
information from the stack is very

167
00:14:50,079 --> 00:14:54,720
helpful as well. For this you would have
to rebuild the wolfTPM library and

168
00:14:54,720 --> 00:14:59,440
install it again. There is a nice option
enable debug. And it has has even

169
00:14:59,440 --> 00:15:04,720
levels of debugging where verbose is the
highest. I think having that just in mind

170
00:15:04,720 --> 00:15:09,639
in case the TPM simulator output is not
enough is good. And then you can always

171
00:15:09,639 --> 00:15:14,959
reach out to us in the OST2 forum. We
have discussion boards after each

172
00:15:14,959 --> 00:15:20,000
section in each exercise so please feel
free to write us any questions and any

173
00:15:20,000 --> 00:15:25,560
difficulties you find doing it right
after the unit or the exercise where you

174
00:15:25,560 --> 00:15:29,920
experience the problem helps us have
more context about the situation and of

175
00:15:29,920 --> 00:15:36,160
course you can always drop us an email
at the course email shown on the

176
00:15:36,160 --> 00:15:39,160
slide

